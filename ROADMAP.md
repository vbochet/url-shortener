# URL Shortener

## MVP 1

- Pas de gestion d'accès : tout le monde peut tout faire
- Pouvoir raccourcir une URL et obtenir un hash généré automatiquement, d'une longueur de 8 à 12 caractères alphanumériques (longueur à définir)
- Pouvoir récupérer une URL longue à partir de son hash

## MVP 2

- Pouvoir choisir son propre hash à la création de l'URL raccourcie (si disponible)
- Pouvoir choisir une durée de vie pour l'URL raccourcie (max 1 an)
- Mettre en place un mécanisme pour supprimer de la base de données toutes les URL raccourcies dont la date d'expiration est dépassée

## MVP 3

- Permettre de définir un mot de passe à la création d'une URL raccourcie
- Ne pas pouvoir récupérer une URL protégée par mot de passe si on ne fournit pas le bon mot de passe

## MVP 4

- Permettre aux utilisateurs de créer un compte pour consulter la liste de leurs URL raccourcies
- Permettre aux utilisateurs connectés avec un compte de créer des URL raccourcies avec une durée de vie plus longue
- Permettre aux utilisateurs connectés avec un compte de supprimer une de leurs URL raccourcies

## MVP 5

- Ajouter des statistiques d'accès aux URL raccourcies
- Permettre de voir les statistiques d'accès d'une URL
