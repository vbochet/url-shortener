CREATE TABLE IF NOT EXISTS `url` (
  `hash` varchar(20) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`hash`)
) DEFAULT CHARSET=utf8;
