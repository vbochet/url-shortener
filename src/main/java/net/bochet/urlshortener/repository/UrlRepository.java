package net.bochet.urlshortener.repository;

import net.bochet.urlshortener.entity.UrlEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface UrlRepository extends JpaRepository<UrlEntity, String> {
    UrlEntity getUrlByHash(String hash);

    List<UrlEntity> findUrlEntitiesByExpirationDateBefore(LocalDate date);
}
