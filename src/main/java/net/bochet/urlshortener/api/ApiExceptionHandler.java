package net.bochet.urlshortener.api;

import net.bochet.urlshortener.dto.ApiErrorDto;
import net.bochet.urlshortener.exception.InvalidHashFormatException;
import net.bochet.urlshortener.exception.InvalidUrlFormatException;
import net.bochet.urlshortener.exception.NoAvailableHashFoundException;
import net.bochet.urlshortener.exception.UrlShortenerException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler({ InvalidUrlFormatException.class, InvalidHashFormatException.class })
    public final ResponseEntity<ApiErrorDto> handleInvalidUrlFormatException(UrlShortenerException ex) {
        return handleException(ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ NoAvailableHashFoundException.class })
    public final ResponseEntity<ApiErrorDto> handleNoHashFoundException(UrlShortenerException ex) {
        return handleException(ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private static ResponseEntity<ApiErrorDto> handleException(UrlShortenerException ex, HttpStatus httpStatus) {
        ApiErrorDto apiErrorDto = new ApiErrorDto();
        apiErrorDto.setMessage(ex.getMessage());
        apiErrorDto.setErrorCode(ex.getCode());
        apiErrorDto.setDate(ex.getDate());

        return new ResponseEntity<>(apiErrorDto, httpStatus);
    }
}
