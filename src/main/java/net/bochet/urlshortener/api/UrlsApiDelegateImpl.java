package net.bochet.urlshortener.api;

import net.bochet.urlshortener.dto.UrlDto;
import net.bochet.urlshortener.exception.InvalidHashFormatException;
import net.bochet.urlshortener.exception.InvalidUrlFormatException;
import net.bochet.urlshortener.exception.NoAvailableHashFoundException;
import net.bochet.urlshortener.service.UrlService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UrlsApiDelegateImpl implements UrlsApiDelegate {
    private final UrlService urlService;

    public UrlsApiDelegateImpl(UrlService urlService) {
        this.urlService = urlService;
    }

    @Override
    public ResponseEntity<String> getUrl(String hash) {
        Optional<String> longUrl = urlService.getLongUrl(hash);
        return longUrl.map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Override
    public ResponseEntity<UrlDto> createUrl(UrlDto urlDto) throws InvalidUrlFormatException, NoAvailableHashFoundException, InvalidHashFormatException {
        String hash;
        if (urlDto.getHash() != null) {
            hash = urlService.createShortUrl(urlDto.getUrl(), urlDto.getHash(), urlDto.getExpirationDate());
        } else {
            hash = urlService.createShortUrl(urlDto.getUrl(), urlDto.getExpirationDate());
        }

        urlDto.setHash(hash);

        return ResponseEntity.ok(urlDto);
    }
}
