package net.bochet.urlshortener.exception;

public class InvalidUrlFormatException extends FuctionalException {
    private final String message;

    public InvalidUrlFormatException(String message) {
        this.message = message;
    }

    @Override
    public String getCode() {
        return "F01";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
