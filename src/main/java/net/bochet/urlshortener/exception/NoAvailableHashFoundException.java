package net.bochet.urlshortener.exception;

public class NoAvailableHashFoundException extends TechnicalException {
    private final String message;

    public NoAvailableHashFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getCode() {
        return "T01";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
