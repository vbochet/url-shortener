package net.bochet.urlshortener.exception;

import java.time.LocalDateTime;

public abstract class UrlShortenerException extends Exception {
    private final LocalDateTime date = LocalDateTime.now();

    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public abstract String getMessage();

    public abstract String getCode();
}
