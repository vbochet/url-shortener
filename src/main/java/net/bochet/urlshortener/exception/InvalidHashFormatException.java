package net.bochet.urlshortener.exception;

public class InvalidHashFormatException extends FuctionalException {
    private final String message;

    public InvalidHashFormatException(String message) {
        this.message = message;
    }

    @Override
    public String getCode() {
        return "F02";
    }

    @Override
    public String getMessage() {
        return message;
    }
}
