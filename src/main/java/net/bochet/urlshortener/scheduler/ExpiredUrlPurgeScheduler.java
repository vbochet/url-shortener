package net.bochet.urlshortener.scheduler;

import net.bochet.urlshortener.entity.UrlEntity;
import net.bochet.urlshortener.repository.UrlRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public class ExpiredUrlPurgeScheduler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExpiredUrlPurgeScheduler.class);

    private final UrlRepository urlRepository;

    public ExpiredUrlPurgeScheduler(UrlRepository urlRepository) {
        this.urlRepository = urlRepository;
    }

    @Scheduled(cron = "${purge.cron}", zone = "${purge.zone}")
    public void purgeExpiredUrls() {
        List<UrlEntity> expiredUrls = urlRepository.findUrlEntitiesByExpirationDateBefore(LocalDate.now());

        LOGGER.info("Suppression des URL expirées : {}", expiredUrls);

        urlRepository.deleteAll(expiredUrls);
    }
}
