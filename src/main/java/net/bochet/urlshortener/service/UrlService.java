package net.bochet.urlshortener.service;

import net.bochet.urlshortener.exception.InvalidHashFormatException;
import net.bochet.urlshortener.exception.InvalidUrlFormatException;
import net.bochet.urlshortener.exception.NoAvailableHashFoundException;

import java.time.LocalDate;
import java.util.Optional;

public interface UrlService {
    String createShortUrl(String url, LocalDate expirationDate) throws InvalidUrlFormatException, NoAvailableHashFoundException, InvalidHashFormatException;

    String createShortUrl(String url, String wishedHash, LocalDate expirationDate) throws InvalidUrlFormatException, NoAvailableHashFoundException, InvalidHashFormatException;

    Optional<String> getLongUrl(String hash);
}
