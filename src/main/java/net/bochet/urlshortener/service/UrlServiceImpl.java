package net.bochet.urlshortener.service;

import net.bochet.urlshortener.entity.UrlEntity;
import net.bochet.urlshortener.exception.InvalidHashFormatException;
import net.bochet.urlshortener.exception.InvalidUrlFormatException;
import net.bochet.urlshortener.exception.NoAvailableHashFoundException;
import net.bochet.urlshortener.repository.UrlRepository;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.zip.CRC32;

@Service
public class UrlServiceImpl implements UrlService {
    private static final short NB_MAX_ATTEMPTS = 5;

    private final UrlRepository urlRepository;

    public UrlServiceImpl(UrlRepository urlRepository) {
        this.urlRepository = urlRepository;
    }


    @Override
    public String createShortUrl(String url, String wishedHash, LocalDate expirationDate) throws InvalidUrlFormatException, NoAvailableHashFoundException, InvalidHashFormatException {
        if (isUrlInvalid(url)) {
            throw new InvalidUrlFormatException("L'URL ne commence pas par http:// ou https://");
        }
        if (isHashInvalid(wishedHash)) {
            throw new InvalidHashFormatException("Le hash respecte pas le format attendu");
        }

        if (wishedHash.length() == 0 || urlRepository.getUrlByHash(wishedHash) != null) {
            return createShortUrl(url, expirationDate); // Le hash souhaité est déjà pris, on attribue donc un autre hash
        }

        UrlEntity newUrlEntity = buildUrlEntity(url, wishedHash, expirationDate);

        newUrlEntity = urlRepository.save(newUrlEntity);

        return newUrlEntity.getHash();
    }

    @Override
    public String createShortUrl(String url, LocalDate expirationDate) throws InvalidUrlFormatException, NoAvailableHashFoundException, InvalidHashFormatException {
        if (isUrlInvalid(url)) {
            throw new InvalidUrlFormatException("L'URL ne commence pas par http:// ou https://");
        }

        short attemptNb = 1;
        UrlEntity newUrlEntity = buildUrlEntity(url, computeHash(url), expirationDate);

        while (urlRepository.getUrlByHash(newUrlEntity.getHash()) != null && attemptNb <= NB_MAX_ATTEMPTS) {
            attemptNb++;
            newUrlEntity.setHash(computeHash(url));
        }

        if (attemptNb > NB_MAX_ATTEMPTS) {
            throw new NoAvailableHashFoundException("Aucun hash disponible n'a pu être trouvé pour cette URL");
        }

        newUrlEntity = urlRepository.save(newUrlEntity);

        return newUrlEntity.getHash();
    }

    private static boolean isUrlInvalid(String url) {
        return url == null || (!url.startsWith("http://") && !url.startsWith("https://"));
    }

    private static boolean isHashInvalid(@NotNull String hash) {
        return hash.length() > 20;
    }

    private static UrlEntity buildUrlEntity(String url, String wishedHash, LocalDate expirationDate) {
        UrlEntity newUrlEntity = new UrlEntity();
        newUrlEntity.setUrl(url);
        newUrlEntity.setHash(wishedHash);
        newUrlEntity.setExpirationDate(expirationDate);
        return newUrlEntity;
    }

    private String computeHash(String url) {
        String valueToHash = url + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME);

        CRC32 crc32 = new CRC32();
        crc32.update(valueToHash.getBytes());

        return Long.toHexString(crc32.getValue());
    }

    @Override
    public Optional<String> getLongUrl(String hash) {
        UrlEntity url = urlRepository.getUrlByHash(hash);

        return Optional.ofNullable(url)
                .filter(u -> !u.isExpired())
                .map(UrlEntity::getUrl);
    }
}
