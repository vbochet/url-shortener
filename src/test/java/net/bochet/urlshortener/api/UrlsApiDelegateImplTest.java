package net.bochet.urlshortener.api;

import net.bochet.urlshortener.dto.UrlDto;
import net.bochet.urlshortener.exception.InvalidHashFormatException;
import net.bochet.urlshortener.exception.InvalidUrlFormatException;
import net.bochet.urlshortener.exception.NoAvailableHashFoundException;
import net.bochet.urlshortener.service.UrlServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class UrlsApiDelegateImplTest {
    @InjectMocks
    private UrlsApiDelegateImpl delegate;
    @Mock
    private UrlServiceImpl urlService;

    @Test
    void get_url_not_found() {
        Mockito.when(urlService.getLongUrl(Mockito.anyString())).thenReturn(Optional.empty());

        ResponseEntity<String> response = delegate.getUrl("testHash");

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    void get_url() {
        String url = "http://random.url";
        Mockito.when(urlService.getLongUrl(Mockito.anyString())).thenReturn(Optional.of(url));

        ResponseEntity<String> response = delegate.getUrl("testHash");

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isEqualTo(url);
    }

    @Test
    void create_url_random_hash() throws InvalidUrlFormatException, NoAvailableHashFoundException, InvalidHashFormatException {
        String hash = "testHash";
        String url = "http://random.url";
        UrlDto urlDto = new UrlDto();
        urlDto.setUrl(url);
        Mockito.when(urlService.createShortUrl(url, null)).thenReturn(hash);

        ResponseEntity<UrlDto> response = delegate.createUrl(urlDto);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
        Assertions.assertThat(response.getBody().getUrl()).isEqualTo(url);
        Assertions.assertThat(response.getBody().getHash()).isEqualTo(hash);
        Assertions.assertThat(response.getBody().getExpirationDate()).isNull();
    }

    @Test
    void create_url_random_hash_expiration_date() throws InvalidUrlFormatException, NoAvailableHashFoundException, InvalidHashFormatException {
        String hash = "testHash";
        String url = "http://random.url";
        LocalDate expirationDate = LocalDate.now();
        UrlDto urlDto = new UrlDto();
        urlDto.setUrl(url);
        urlDto.setExpirationDate(expirationDate);
        Mockito.when(urlService.createShortUrl(url, expirationDate)).thenReturn(hash);

        ResponseEntity<UrlDto> response = delegate.createUrl(urlDto);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
        Assertions.assertThat(response.getBody().getUrl()).isEqualTo(url);
        Assertions.assertThat(response.getBody().getHash()).isEqualTo(hash);
        Assertions.assertThat(response.getBody().getExpirationDate()).isEqualTo(expirationDate);
    }

    @Test
    void create_url_wished_hash() throws InvalidUrlFormatException, NoAvailableHashFoundException, InvalidHashFormatException {
        String hash = "wishedHash";
        String url = "http://random.url";
        UrlDto urlDto = new UrlDto();
        urlDto.setUrl(url);
        urlDto.setHash(hash);
        Mockito.when(urlService.createShortUrl(url, hash, null)).thenReturn(hash);

        ResponseEntity<UrlDto> response = delegate.createUrl(urlDto);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
        Assertions.assertThat(response.getBody().getUrl()).isEqualTo(url);
        Assertions.assertThat(response.getBody().getHash()).isEqualTo(hash);
        Assertions.assertThat(response.getBody().getExpirationDate()).isNull();
    }

    @Test
    void create_url_wished_hash_expiration_date() throws InvalidUrlFormatException, NoAvailableHashFoundException, InvalidHashFormatException {
        String hash = "wishedHash";
        String url = "http://random.url";
        LocalDate expirationDate = LocalDate.now();
        UrlDto urlDto = new UrlDto();
        urlDto.setUrl(url);
        urlDto.setHash(hash);
        urlDto.setExpirationDate(expirationDate);
        Mockito.when(urlService.createShortUrl(url, hash, expirationDate)).thenReturn(hash);

        ResponseEntity<UrlDto> response = delegate.createUrl(urlDto);

        Assertions.assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        Assertions.assertThat(response.getBody()).isNotNull();
        Assertions.assertThat(response.getBody().getUrl()).isEqualTo(url);
        Assertions.assertThat(response.getBody().getHash()).isEqualTo(hash);
        Assertions.assertThat(response.getBody().getExpirationDate()).isEqualTo(expirationDate);
    }
}
