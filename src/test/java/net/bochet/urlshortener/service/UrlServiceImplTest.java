package net.bochet.urlshortener.service;

import net.bochet.urlshortener.entity.UrlEntity;
import net.bochet.urlshortener.exception.InvalidHashFormatException;
import net.bochet.urlshortener.exception.InvalidUrlFormatException;
import net.bochet.urlshortener.exception.NoAvailableHashFoundException;
import net.bochet.urlshortener.repository.UrlRepository;
import org.assertj.core.api.Assertions;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalMatchers;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class UrlServiceImplTest {
    @InjectMocks
    private UrlServiceImpl urlService;
    @Mock
    private UrlRepository urlRepository;

    @Test
    void get_long_url_not_found() {
        Mockito.when(urlRepository.getUrlByHash(Mockito.anyString())).thenReturn(null);

        Optional<String> longUrl = urlService.getLongUrl("test");

        Assertions.assertThat(longUrl).isEmpty();
    }

    @Test
    void get_long_url_expired() {
        UrlEntity urlEntity = new UrlEntity();
        urlEntity.setUrl("url");
        urlEntity.setHash("test");
        urlEntity.setExpirationDate(LocalDate.of(2000,1,1));
        Mockito.when(urlRepository.getUrlByHash(Mockito.anyString())).thenReturn(urlEntity);

        Optional<String> longUrl = urlService.getLongUrl("test");

        Assertions.assertThat(longUrl).isEmpty();
    }

    @Test
    void get_long_url() {
        UrlEntity urlEntity = new UrlEntity();
        urlEntity.setUrl("url");
        urlEntity.setHash("test");
        Mockito.when(urlRepository.getUrlByHash(Mockito.anyString())).thenReturn(urlEntity);

        Optional<String> longUrl = urlService.getLongUrl("test");

        Assertions.assertThat(longUrl)
                .isNotEmpty()
                .contains(urlEntity.getUrl());
    }

    @Test
    void get_long_url_not_expired() {
        UrlEntity urlEntity = new UrlEntity();
        urlEntity.setUrl("url");
        urlEntity.setHash("test");
        urlEntity.setExpirationDate(LocalDate.now().plusDays(7));
        Mockito.when(urlRepository.getUrlByHash(Mockito.anyString())).thenReturn(urlEntity);

        Optional<String> longUrl = urlService.getLongUrl("test");

        Assertions.assertThat(longUrl)
                .isNotEmpty()
                .contains(urlEntity.getUrl());
    }

    @Test
    void create_short_url_url_invalid() {
        Assertions.assertThatThrownBy(() -> urlService.createShortUrl("invalidUrl", null))
                .isInstanceOf(InvalidUrlFormatException.class);

        Assertions.assertThatThrownBy(() -> urlService.createShortUrl("invalidUrl", "hash", null))
                .isInstanceOf(InvalidUrlFormatException.class);
    }

    @Test
    void create_short_url_max_attempt_reached() {
        Mockito.when(urlRepository.getUrlByHash(Mockito.anyString())).thenReturn(new UrlEntity());

        Assertions.assertThatThrownBy(() -> urlService.createShortUrl("http://random.url", null))
                .isInstanceOf(NoAvailableHashFoundException.class);
    }

    @Test
    void create_short_url_random_hash() throws InvalidUrlFormatException, NoAvailableHashFoundException, InvalidHashFormatException {
        UrlEntity urlEntity = new UrlEntity();
        urlEntity.setHash("testHash");

        Mockito.when(urlRepository.getUrlByHash(Mockito.anyString())).thenReturn(null);
        Mockito.when(urlRepository.save(Mockito.any())).thenReturn(urlEntity);

        String hash = urlService.createShortUrl("http://random.url", null);

        Assertions.assertThat(hash).isEqualTo(urlEntity.getHash());
    }

    @Test
    void create_short_url_hash_invalid() {
        Assertions.assertThatThrownBy(() -> urlService.createShortUrl("https://test.fr", "123456789012345678901", null))
                .isInstanceOf(InvalidHashFormatException.class);
    }

    @Test
    void create_short_url_wished_hash_empty() throws InvalidUrlFormatException, InvalidHashFormatException, NoAvailableHashFoundException {
        String url = "https://test.fr";
        String hash = "testHash";
        UrlEntity urlEntity = new UrlEntity();
        urlEntity.setHash(hash);

        Mockito.when(urlRepository.getUrlByHash(Mockito.anyString())).thenReturn(null);
        Mockito.when(urlRepository.save(Mockito.any())).thenReturn(urlEntity);

        String generatedHash = urlService.createShortUrl(url, "", null);

        Assertions.assertThat(generatedHash).isEqualTo(hash);
    }

    @Test
    void create_short_url_wished_hash_already_used() throws InvalidUrlFormatException, InvalidHashFormatException, NoAvailableHashFoundException {
        String url = "https://test.fr";
        String wishedHash = "wishedHash";
        String randomHash = "randomHash";
        UrlEntity existingUrlEntity = new UrlEntity();
        existingUrlEntity.setHash(wishedHash);
        UrlEntity newUrlEntity = new UrlEntity();
        newUrlEntity.setHash(randomHash);

        Mockito.when(urlRepository.getUrlByHash(Mockito.anyString())).thenReturn(existingUrlEntity).thenReturn(null);
        Mockito.when(urlRepository.save(Mockito.any())).thenReturn(newUrlEntity);

        String generatedHash = urlService.createShortUrl(url, "", null);

        Assertions.assertThat(generatedHash).isEqualTo(randomHash);
    }

    @Test
    void create_short_url_wished_hash() throws InvalidUrlFormatException, InvalidHashFormatException, NoAvailableHashFoundException {
        String url = "https://test.fr";
        String wishedHash = "wishedHash";
        UrlEntity newUrlEntity = new UrlEntity();
        newUrlEntity.setHash(wishedHash);

        Mockito.when(urlRepository.getUrlByHash(Mockito.anyString())).thenReturn(null);
        Mockito.when(urlRepository.save(Mockito.any())).thenReturn(newUrlEntity);

        String generatedHash = urlService.createShortUrl(url, wishedHash, null);

        Assertions.assertThat(generatedHash).isEqualTo(wishedHash);
    }
}
