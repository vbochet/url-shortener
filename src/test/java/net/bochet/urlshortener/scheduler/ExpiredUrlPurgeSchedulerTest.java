package net.bochet.urlshortener.scheduler;

import net.bochet.urlshortener.entity.UrlEntity;
import net.bochet.urlshortener.exception.InvalidHashFormatException;
import net.bochet.urlshortener.exception.InvalidUrlFormatException;
import net.bochet.urlshortener.exception.NoAvailableHashFoundException;
import net.bochet.urlshortener.repository.UrlRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class ExpiredUrlPurgeSchedulerTest {
    @InjectMocks
    private ExpiredUrlPurgeScheduler purgeScheduler;
    @Mock
    private UrlRepository urlRepository;

    @Test
    void purge_expired_urls() {
        UrlEntity url1 = new UrlEntity();
        url1.setHash("1");
        UrlEntity url2 = new UrlEntity();
        url2.setHash("2");
        List<UrlEntity> expiredUrls = List.of(url1, url2);
        Mockito.when(urlRepository.findUrlEntitiesByExpirationDateBefore(Mockito.any())).thenReturn(expiredUrls);

        purgeScheduler.purgeExpiredUrls();

        ArgumentCaptor<List<UrlEntity>> argumentCaptor = ArgumentCaptor.forClass(List.class);
        Mockito.verify(urlRepository, Mockito.times(1)).deleteAll(argumentCaptor.capture());
        Assertions.assertThat(argumentCaptor.getValue()).containsExactlyInAnyOrderElementsOf(expiredUrls);
    }
}
