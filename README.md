# URL Shortener

## Stack technique

- Java 17
- Maven 3.8.6

## Build

Se placer dans le répertoire racine du projet et exécuter la commmande `mvn clean install`. Cela permettra de générer les classes de DTO et les contrôleurs à partir de la spécification OpenAPI.

## Lancer l'application

Se placer dans le répertoire racine du projet et exécuter la commande `mvn spring-boot:run -Dspring-boot.run.profiles=<env>` en remplaçant `<env>` par l'environnement sur lequel est lancée l'application (valeurs possibles : `dev`, `int`, `rec`, `prod`).
